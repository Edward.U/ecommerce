// Library Imports
import {Fragment, useContext, useState, useEffect} from 'react';
import {Container, Nav, Navbar} from 'react-bootstrap';
import {Link, NavLink} from 'react-router-dom';


// Components Imports
import UserContext from '../UserContext.js';

export default function AppNavBar(){

	const {user} = useContext(UserContext);
	// const [isAdmin, setIsAdmin] = useState("");

	// useEffect(() => {
	// 	if(user.isAdmin == false || user.isAdmin == "" || user.isAdmin == null){
	// 		setIsAdmin(false);
	// 	}
	// 	else{
	// 		setIsAdmin(true);
	// 	}
	// })

	return(
		<Navbar bg="light" expand="lg">
		      <Container>
		        
		      	{
		      		user && user.isAdmin?
		      		<Navbar.Brand as = {Link} to = "/admin" className = "logo">SOOBOONIT</Navbar.Brand>
		      		:
		      		<Navbar.Brand as = {Link} to = "/" className = "logo">SOOBOONIT</Navbar.Brand>
		      	}

		        <Navbar.Toggle aria-controls="basic-navbar-nav" />
		        <Navbar.Collapse id="basic-navbar-nav">
		          <Nav className="ms-auto">

		          	{/*Conditional Rendering for home and Admin Dashboard*/}
		          	{
		          		user && user.isAdmin?
		          		<Nav.Link as = {NavLink} to = "/admin">Admin Dashboard</Nav.Link>
		          		:
		          		<Nav.Link as = {NavLink} to = "/" >Home</Nav.Link>
		          	}
		            
		            <Nav.Link as = {NavLink} to = "/products/showActiveProducts" >Products</Nav.Link>  

		            {/*Conditional Rendering for Login, Register, and Logout*/}
		            {
		            	user ?
		                <Nav.Link as = {NavLink} to = "/user/logout">Logout</Nav.Link>
		                :
		                <Fragment>
		            		<Nav.Link as = {NavLink} to = "/user/login">Login</Nav.Link>
		            		<Nav.Link as = {NavLink} to = "/user/register">Register</Nav.Link>
		            	</Fragment>
		            }

		          </Nav>
		        </Navbar.Collapse>
		      </Container>
		    </Navbar>
		);
}