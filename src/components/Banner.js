// Library Imports
import {Container, Row, Col, Button} from 'react-bootstrap';
import {useContext} from 'react';
import {Link} from 'react-router-dom';


// Components Imports
import UserContext from '../UserContext.js';

export default function Banner(){

	const {user} = useContext(UserContext);

	return(
		<Container className = "col-lg-5 col-12">
			<Row className = "mt-5">
				<Col className = "text-center">
					<h1 className = "title">SOOBOONIT</h1>
					<p className = "subtitle">Your one-stop shop for merch na presyong sulit!</p>

					{
						user?
						<Button as  = {Link} to = {`/products/showActiveProducts`}>Shop Now</Button>
						:
						<Button as  = {Link} to = {`/user/login`}>Shop Now</Button>
					}

				</Col>
			</Row>
		</Container>
		);
}