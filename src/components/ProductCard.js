// Library Imports
import {Button, Card} from 'react-bootstrap';
import {useContext} from 'react';
import {Link} from 'react-router-dom';


// Components Imports
import UserContext from '../UserContext.js';


export default function ProductCard({productProp}){
// export default function ProductCard(){

	const {_id, name, price, image} = productProp;

	const {user} = useContext(UserContext);

	return(
		// <Container className = "bg-primary mt-5 col-4 col-lg-4">
		      <Card className = "mx-3 my-3 col-9 col-md-4 col-lg-3 mh-100">
		            <Card.Body>
		            	<Card.Img variant="top" src={image} />
		                <Card.Title className = "mt-4">{name}</Card.Title>

		                <Card.Subtitle className = "mt-2">Price: Php {price}</Card.Subtitle>

		                  {
		                  	user?
		                  	<Button as = {Link} to = {`/products/showProduct/${_id}`} className = "mt-3">View item</Button>
		                  	:
		                  	<Button as = {Link} to = {`/user/login`} className = "mt-3">Login</Button>
		                  }

		            </Card.Body>
		          </Card>
		// </Container>
		);
}