// Library Imports
import {Table} from 'react-bootstrap';
import {useEffect, useState, Fragment} from 'react';


// Components Imports
import ProductTable from './ProductTable.js';


export default function ShowAllProducts(){

	const [products, setProducts] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_SOOBOONIT}/products/showAllProducts`)
		.then(result => result.json())
		.then(data => {
			// console.log(data);

			setProducts(data.map(product => {
				return(
					<ProductTable key = {product._id} productProp = {product} />
					)
			}))
		})
	})

	return(
		<Table striped bordered hover className = "my-5">
		      <thead>
		        <tr>
		          <th>Product Name</th>
		          <th>Description</th>
		          <th>Price</th>
		          <th>Availability</th>
		          <th>Actions</th>
		        </tr>
		      </thead>

		      <tbody>
			      <Fragment>
			      	{products}
			      </Fragment>
		      </tbody>
		</Table>
		);
}