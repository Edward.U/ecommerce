// Library Imports
import {Fragment, useState, useEffect} from 'react';
import {Container, Table} from 'react-bootstrap';

// Components Imports
import OrderTable from './OrderTable.js';


export default function ViewOrders(){

	const [orders, setOrders] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_SOOBOONIT}/order/ViewAllOrders`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(result => result.json())
		.then(data => {
			// console.log(data);
			// console.log(typeof data);

			setOrders(data.map(order => {
				return(
					<OrderTable key = {order._id} orderProp = {order} />
					)
			}))
		})
	}, [])

	return(
		<Fragment>
			<h1 className = "text-center mt-5">All Orders</h1>

			<Container className = "mt-2 col-lg-9 col-9 d-flex flex-wrap justify-content-center">
				<Table striped bordered hover className = "my-5">
				      <thead>
				        <tr>
				          <th>Order ID</th>
				          <th>User ID</th>
				          <th>Product ID</th>
				          <th>Quantity</th>
				          <th>Total Amount</th>
				          <th>Date Ordered</th>
				        </tr>
				      </thead>

				      <tbody>
					      <Fragment>
					      	{orders}
					      </Fragment>
				      </tbody>
				</Table>
			</Container>
		</Fragment>
		);
}