// Library Imports
import {Container, Button, Form, Table, Modal} from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import {useState, useEffect, Fragment, useContext} from 'react';
import Swal from 'sweetalert2';

// Components Imports
import UserContext from '../UserContext.js';


export default function ProductView(){

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [count, setCount] = useState(0);
	const [setTotalAmount] = useState('');

	const {user} = useContext(UserContext);

	const {productId} = useParams();
	const navigate = useNavigate();


	// For Modal
	const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	// function addCount(){
	// 	setCount(count + 1);
	// 	// console.log(count);
	// }

	// function subtractCount(){
	// 	if(count > 0){
	// 		setCount(count - 1);
	// 		// console.log(count);
	// 	}
	// 	else{
	// 		// console.log(count);
	// 	}
	// }

	useEffect(() => {
		// console.log(productId);

		fetch(`${process.env.REACT_APP_SOOBOONIT}/products/showProduct/${productId}`)
		.then(result => result.json())
		.then(data => {
			// console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	}, [productId]);



	function purchase (event){
		fetch(`${process.env.REACT_APP_SOOBOONIT}/order/createOrder`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				_id: productId,
				quantity: count
			})
		})
		.then(result => result.json())
		.then(data => {
			// console.log(data);

			if(data){
				Swal.fire({
					title: "Order successful!",
					icon:'success',
					text: "Thank you for ordering at SOOBOONIT!"
				})

				navigate('/products/showActiveProducts');
			}
			else{
				Swal.fire({
					title: "Order failed",
					icon: 'error',
					text: "Please try again."
				})
			}
		})
	}


	return(
		// <Container className = "col-9 text-center">
		// 	<Table striped bordered hover size="sm" className = "my-5">
		// 		<thead>
		// 	        <tr>
		// 	          <th>Product Name</th>
		// 	          <th>Description</th>
		// 	          <th>Price</th>
		// 	          <th>Quantity</th>
		// 	          <th>Total</th>
		// 	          <th>Action</th>
		// 	        </tr>
		// 	      </thead>

		// 		<tbody>
		// 		  <tr>
		// 		    <td>{name}</td>
		// 		    <td>{description}</td>
		// 		    <td>Php {price}</td>
		// 		    <td>
		// 		    	<Button onClick = {subtractCount} className = "mx-2">-</Button>
		// 		    	{count}
		// 		    	<Button onClick = {addCount} className = "mx-2">+</Button>
		// 		    </td>
		// 		    <td>{price * count}</td>

		// 		    <td>
		// 		  		<Button className = "mt-3" onClick = {purchase}>Checkout</Button>
		// 		  	</td>
		// 		  </tr>

		// 		</tbody>
		// 	</Table>
		// </Container>

		<Container className = "col-9 text-center">
			<Table striped bordered hover size="sm" className = "my-5">
				<thead>
			        <tr>
			          <th>Product Name</th>
			          <th>Description</th>
			          <th>Price</th>
			          <th>Action</th>
			        </tr>
			      </thead>

				<tbody>
				  <tr>
				    <td>{name}</td>
				    <td>{description}</td>
				    <td>Php {price}</td>
				    
				    {
				    	user&&user.isAdmin?
				    	<td>
				  			<Button as = {Link} to = "/admin">Admin Dashboard</Button>
				  		</td>
				  		:
				  		<td>
				  			<Button onClick = {handleShow}>Checkout</Button>
				  		</td>
				    }

				  </tr>

				</tbody>
			</Table>


			<Fragment>
				<Modal
				   show={show}
				   onHide={handleClose}
				   backdrop="static"
				   keyboard={false}
				 >
				   <Modal.Header closeButton>
				     <Modal.Title>Order Form</Modal.Title>
				   </Modal.Header>
				   <Modal.Body>
				   		<Form>
				   		    <Form.Group className="mb-3" controlId="formBasicEmail">
				   		        <Form.Label>Product Name</Form.Label>
				   		        <Form.Control
				   		        type="text"
				   		        placeholder = "Enter Updated Name"
				   		        value = {name}
				   		        onChange = {event => setName(event.target.value)}
				   		        required
				   		        disabled />
				   		    </Form.Group>

				   		    <Form.Group className="mb-3" controlId="formBasicPassword">
				   		        <Form.Label>Product Description</Form.Label>
				   		        <Form.Control
				   		        type="text"
				   		        placeholder = "Enter Updated Description"
				   		        value = {description}
				   		        onChange = {event => setDescription(event.target.value)}
				   		        required
				   		        disabled />
				   		    </Form.Group>

				   		    <Form.Group className="mb-3" controlId="formBasicPassword">
				   		        <Form.Label>Price</Form.Label>
				   		        <Form.Control
				   		        type="number"
				   		        placeholder = "Enter Updated Price"
				   		        value = {price}
				   		        onChange = {event => setPrice(event.target.value)}
				   		        disabled
				   		        required/>
				   		    </Form.Group>

				   		    <Form.Group className="mb-3" controlId="formBasicPassword">
				   		        <Form.Label>Quantity</Form.Label>
				   		        <Form.Control
				   		        type="number"
				   		        min = "0"
				   		        placeholder = "Enter Updated Price"
				   		        value = {count}
				   		        onChange = {event => setCount(event.target.value)}
				   		        required/>
				   		        {/*<Button onClick = {subtractCount} className = "mx-2">-</Button>
				   		        <Button onClick = {addCount} className = "mx-2">+</Button>*/}
				   		    </Form.Group>

				   		    <Form.Group className="mb-3" controlId="formBasicPassword">
				   		        <Form.Label>Total</Form.Label>
				   		        <Form.Control
				   		        type="number"
				   		        placeholder = "Enter Desired Quantity"
				   		        value = {price * count}
				   		        onChange = {event => setTotalAmount(event.target.value)}
				   		        required
				   		        disabled />
				   		    </Form.Group>

				   		</Form>
				   </Modal.Body>
				   <Modal.Footer>
				     <Button variant="secondary" onClick={handleClose}>Cancel</Button>
				     
				     {
				     	count===0?
				     	<Button variant="primary" onClick = {purchase} disabled>Order</Button>
				     	:
				     	<Button variant="primary" onClick = {purchase}>Order</Button>
				     }

				   </Modal.Footer>
				</Modal>
			</Fragment>


		</Container>


		// Modal
		
		)
}