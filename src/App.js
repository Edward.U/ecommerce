// src imports
import './App.css';


// components imports
import AppNavBar from './components/AppNavBar.js';
import Home from './pages/Home.js';
import Login from './pages/Login.js';
import Register from './pages/Register.js';
import Error from './pages/Error.js';
import Logout from './pages/Logout.js';
import {UserProvider} from './UserContext.js';
import Products from './pages/Products.js';
import ProductView from './components/ProductView.js';
import AdminDashboard from './components/AdminDashboard.js';
import AddProduct from './pages/AddProduct.js';
import ViewOrders from './components/ViewOrders.js';


// Library Imports
import {
  BrowserRouter as Router,
  Routes,
  Route
} from 'react-router-dom';
import {useState, useEffect} from 'react';




function App() {

  const [user, setUser] = useState(null);

  useEffect(() => {
    // console.log(user);
  }, [user]);

  const unSetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_SOOBOONIT}/user/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(result => result.json())
    .then(data => {
      // console.log(data);

      if(localStorage.getItem('token') !== null){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      }
      else{
          setUser(null);
      }
    })
  }, [])


  return(
    <Router>
      <UserProvider value = {{user, setUser, unSetUser}}>
        <AppNavBar/>

        <Routes>
          <Route path = "/" element = {<Home/>} />

          <Route path = "/user/login" element = {<Login/>} />
          <Route path = "/user/register" element = {<Register/>} />
          <Route path = "/user/logout" element = {<Logout/>} />

          <Route path = "/products/showActiveProducts" element = {<Products/>} />
          <Route path = "/products/showProduct/:productId" element = {<ProductView/>} />
          
          <Route path = "/admin" element = {<AdminDashboard/>} />
          <Route path = "/products/addProduct" element = {<AddProduct/>} />

          <Route path = "/order/viewAllOrders" element = {<ViewOrders/>} />

          <Route path = "*" element = {<Error/>} />
        </Routes>
      </UserProvider>
    </Router> 
    );
}

export default App;
