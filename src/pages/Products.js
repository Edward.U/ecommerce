// Library Imports
import {Fragment, useState, useEffect} from 'react';
import {Container} from 'react-bootstrap';

// Components Imports
import ProductCard from "../components/ProductCard.js";


export default function Products(){

	const [products, setProducts] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_SOOBOONIT}/products/showActiveProducts`)
		.then(result => result.json())
		.then(data => {
			// console.log(data);
			// console.log(typeof data);

			setProducts(data.map(product => {
				return(
						<ProductCard key = {product._id} productProp = {product} />
					)
			}))
		})
	}, [])

	return(
		<Fragment>
			<h1 className = "text-center mt-5">Products</h1>
			
			<Container fluid className = "mt-5 col-lg-9 col-9 d-flex flex-wrap justify-content-center">
				{products}
			</Container>
		</Fragment>
		);
}