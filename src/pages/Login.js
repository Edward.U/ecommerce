// Library Imports
import {useState, useContext} from 'react';
import {Container, Col, Form, Button} from 'react-bootstrap';
import {useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';


// Components Imports
import UserContext from '../UserContext.js';


export default function Login(){

// useStates
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const navigate = useNavigate();
	const {setUser} = useContext(UserContext);
	// console.log(user);


// useEffects OR functions
	// useEffect(() => {
	// 	console.log(email);
	// 	console.log(password);
	// }, [email, password])


	function login(event){
		event.preventDefault();


		fetch(`${process.env.REACT_APP_SOOBOONIT}/user/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(result => result.json())
		.then(data => {
			// console.log(data);

			if(data === false){
				Swal.fire({
					title: "Login failed",
					icon: 'error',
					text: "Please try again"
				})
			}
			else{
				localStorage.setItem('token', data.auth);
				retrieveUserDetails(localStorage.getItem('token'));

				Swal.fire({
					title: "Login successful",
					icon: 'success',
					text: "Enjoy shopping with SOOBOONIT!"
				})

				navigate('/');
			}
		})

		

		// localStorage.setItem("email", email);

		// alert("Logged in successfully");
		// setEmail('');
		// setPassword('');
	}


	const retrieveUserDetails = (token) => {
		fetch(`${process.env.REACT_APP_SOOBOONIT}/user/details`, {
			headers:{
				Authorization: `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(data => {
			// console.log(data);

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})

			// Conditional Navigation for Admin and Non Admin Users
			
				data.isAdmin?
				navigate("/admin")
				:
				navigate("/")
			
		})
	}


	return(
		// user?
		// <Navigate to = "/*" />
		// :
		<Container className = "mt-5 col-lg-3">
			<Col className = "text-center">
				<p className = "title">SOOBOONIT</p>
			</Col>
			<Form>
			      <Form.Group className="mb-3" controlId="formBasicEmail">
			        <Form.Label>Email address</Form.Label>
			        <Form.Control
			        	type="email"
			        	placeholder="Enter email"
			        	value = {email}
			        	onChange = {event => setEmail(event.target.value)}
			        	required />
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="formBasicPassword">
			        <Form.Label>Password</Form.Label>
			        <Form.Control
			        	type="password"
			        	placeholder="Password"
			        	value = {password}
			        	onChange = {event => setPassword(event.target.value)}
			        	required />
			      </Form.Group>
			      
			      <Button variant="primary" type="submit" onClick = {login}>
			        Login
			      </Button>
			    </Form>
		</Container>
		);
}