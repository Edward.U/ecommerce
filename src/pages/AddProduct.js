// Library Imports
import {Container, Col, Form, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import Swal from 'sweetalert2';
import {Navigate, useNavigate} from 'react-router-dom';


// Components Imports
import UserContext from "../UserContext.js";


export default function AddProduct(){

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	const [image, setImage] = useState("");

	const [isActive, setIsActive] = useState(false);

	const navigate = useNavigate();
	const {user, setUser} = useContext(UserContext);

	useEffect(() => {
		if(name !== "" && description !== "" && price !== ""){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	}, [name, description, price])


	function addProduct (event){
		event.preventDefault();

		fetch(`${process.env.REACT_APP_SOOBOONIT}/products/addProduct`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				image: image
			})
		})
		.then(result => result.json())
		.then(data => {
			// console.log(data);

			// retrieveUserDetails(localStorage.getItem('token'));

			if(data){
				Swal.fire({
					title: "New Product Creation Done",
					icon: 'success',
					text: "You can view your newly added product in the products list."
				})

				navigate('/admin');
			}
			else{
				Swal.fire({
					title: "Add Product failed",
					icon: "error",
					text: "Please try again"
				})
			}
		})
	}


	return(
		<Container className = "my-5 col-lg-3">
			<Col className = "text-center">
				<p className = "title">SOOBOONIT</p>
				<p className = "subtitle">Add New Product</p>
			</Col>
			<Form onSubmit = {event => addProduct(event)}>
			    <Form.Group className="mb-3" controlId="formBasicEmail">
			        <Form.Label>Product Name</Form.Label>
			        <Form.Control
			        type="text"
			        placeholder="Enter product name"
			        value = {name}
			        onChange = {event => setName(event.target.value)}
			        required/>
			    </Form.Group>

			    <Form.Group className="mb-3" controlId="formBasicPassword">
			        <Form.Label>Product Description</Form.Label>
			        <Form.Control
			        type="text"
			        placeholder="Enter product description"
			        value = {description}
			        onChange = {event => setDescription(event.target.value)}
			        required/>
			    </Form.Group>

			    <Form.Group className="mb-3" controlId="formBasicPassword">
			        <Form.Label>Price</Form.Label>
			        <Form.Control
			        type="number"
			        min = "0"
			        placeholder="Enter price"
			        value = {price}
			        onChange = {event => setPrice(event.target.value)}
			        required/>
			    </Form.Group>

			    <Form.Group className="mb-3" controlId="formBasicPassword">
			        <Form.Label>Image Link</Form.Label>
			        <Form.Control
			        type="text"
			        placeholder="Enter image link"
			        value = {image}
			        onChange = {event => setImage(event.target.value)}
			        required/>
			    </Form.Group>

			    <Button variant="success" type="submit" onClick = {addProduct}>
			        Add Product
			    </Button>
			</Form>
		</Container>
		);
}