// Library Imports
import {Navigate} from 'react-router-dom';
import {Fragment, useContext, useEffect} from 'react';


// Components Imports
import UserContext from '../UserContext.js';


export default function Logout(){


	const {unSetUser, setUser} = useContext(UserContext);

	useEffect(() => {
		unSetUser();
		setUser(null);
	}, [])

	return(
		<Navigate to = "/user/login" />
		);
}