// Library Imports
import {Fragment} from 'react';
import {Link} from 'react-router-dom'
import {Row, Col} from 'react-bootstrap'


export default function Error(){

	return(
		<Row className = "mt-3 text-center">
			<Col className = "col-md-6 col-10 mx-auto p-3">
				<Fragment>
					<p className = "title">Page Not Found</p>
					<p className = "subtitle">Go back to the <Link to = "/">homepage</Link></p>
				</Fragment>
			</Col>
		</Row>
		);
}