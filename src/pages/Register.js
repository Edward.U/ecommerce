// Library Imports
import {useState, useEffect, useContext} from 'react';
import {Navigate, useNavigate} from 'react-router-dom';
import {Container, Col, Button, Form} from 'react-bootstrap';
import Swal from 'sweetalert2';


// Components Imports
import UserContext from '../UserContext.js';


export default function Register(){

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	const navigate = useNavigate();
	const {user} = useContext(UserContext);

	const [isActive, setIsActive] = useState(false);

	// useEffect(() => {
	// 	console.log(email);
	// 	console.log(password);
	// }, [email, password])


	useEffect(() => {
		if(email !== "" && password !== ""){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	}, [email, password])

	function register(event){
		event.preventDefault();

		fetch(`${process.env.REACT_APP_SOOBOONIT}/user/register`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(result => result.json())
		.then(data => {
			// console.log(data);

			if(data){
				Swal.fire({
					title: "Registration successful",
					icon: 'success',
					text: "You can now login your account"
				})

				navigate('/user/login');
			}
			else{
				Swal.fire({
					title: "Registration failed",
					icon: "error",
					text: "Please try again"
				})
			}
		})
	}

	return(
		user?
		<Navigate to = "/*" />
		:
		<Container className = "mt-5 col-lg-3">
			<Col className = "text-center">
				<p className = "title">SOOBOONIT</p>
				<p className = "subtitle">Sign Up</p>
			</Col>
			<Form>
			  <Form.Group className="mb-3" controlId="formBasicEmail">
			    <Form.Label>Email address</Form.Label>
			    <Form.Control
				    type="email"
				    placeholder="Enter email"
				    value = {email}
				    onChange = {event => setEmail(event.target.value)}
				    required />
			  </Form.Group>

			  <Form.Group className="mb-3" controlId="formBasicPassword">
			    <Form.Label>Password</Form.Label>
			    <Form.Control
			    	type="password"
			    	placeholder="Password"
			    	value = {password}
			    	onChange = {event => setPassword(event.target.value)}
			    	required />
			  </Form.Group>
			  

			  {/*Conditional Rendering for isActive*/}
			  {
			  	isActive?
			  	<Button variant="primary" type="submit" onClick = {register}>
			  	  Sign Up
			  	</Button>
			  	:
			  	<Button variant="danger" type="submit" disabled>
			  	  Sign Up
			  	</Button>

			  }

			</Form>
		</Container>
		);
}